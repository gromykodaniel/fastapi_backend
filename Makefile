.PHONY: install-deps start-docker run-uvicorn

install-deps:
	poetry install

start-docker:
	docker-compose up -d

run-uvicorn:
	poetry run uvicorn src.main:app --reload

all: install-deps start-docker run-uvicorn
