from sqlalchemy.ext.asyncio import create_async_engine, async_sessionmaker , AsyncSession
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy.ext.declarative import declarative_base
from src.config import settings

DATABASE_URL = settings.DATABASE_URL
DATABASE_PARAMS = {}

engine = create_async_engine(DATABASE_URL, echo=True)

async_session_maker = async_sessionmaker(engine, expire_on_commit=False , class_=AsyncSession )
Base = declarative_base()