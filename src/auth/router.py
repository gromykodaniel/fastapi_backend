from fastapi import APIRouter
from sqlalchemy import text
from src.database import async_session_maker

router = APIRouter(prefix="", tags=["Контракты аутентификация"])



@router.post("/register’", status_code=200)
#Как я понял пока без валидации почты (?)
async def register_user(username : str , email  : str , password  :  str  ):
    # Будет await функция которая обработает данные и вернет ответ

    # async with async_session_maker() as session :
    #     try:await session.execute( text("SELECT 1") )
    #     except:return {'database':'not working'}

    return {"username" : username ,"email"  : email , "id": 1   }


@router.post("/login’", status_code=200)
async  def login_user(username : str , password  :  str):


    #https: // fastapi.tiangolo.com / tutorial / security / simple - oauth2 /
    # user_dict = fake_users_db.get(form_data.username)
    # if not user_dict:
    #     raise HTTPException(status_code=400, detail="Incorrect username or password")
    # user = UserInDB(**user_dict)
    # hashed_password = fake_hash_password(form_data.password)
    # if not hashed_password == user.hashed_password:
    #     raise HTTPException(status_code=400, detail="Incorrect username or password")
    #
    # return {"access_token": user.username, "token_type": "bearer"}

    return {"access_token": username, "token_type": "bearer"}

@router.get("/users/me")
async def read_users_me(
    #current_user: Annotated[User, Depends(get_current_active_user)],
    current_user: str ,
):
    return current_user

@router.post("/confirm-code")
async  def confirm(code:str) :

#будет проверка ввода кода с отправленым кодом
    if len(code)==6: return {'message' : 'email подтвержден'}
    return {'message' : 'email не подтвержден'}

@router.post("/reset-password-code")
async def reset_psw_code(email:str):
#await отправка сообщения на почту
    return {'message': 'сообщение отправлено на почту'}


@router.post("/reset-password")
async def reset_psw(email:str , code:str  ):
#await изменение пароля в бд
    return {'message': 'новый пароль готов'}



#Потом уберу отсюда
@router.post("/ping", status_code=200)
async def PING_DB( ):


    async with async_session_maker() as session :
        try:
            await session.execute( text("SELECT 1") )
            return {'success!':' database are working'}
        except:return {'database':'not working'}

